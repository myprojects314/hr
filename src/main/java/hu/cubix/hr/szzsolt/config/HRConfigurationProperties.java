package hu.cubix.hr.szzsolt.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "hr")
@Component
public class HRConfigurationProperties {
    private Raise raise;

    public Raise getRaise() {
        return raise;
    }

    public void setRaise(Raise raise) {
        this.raise = raise;
    }

    public static class Raise {
        private Special special;

        public Special getSpecial() {
            return special;
        }

        public void setSpecial(Special special) {
            this.special = special;
        }

        public static class Special {
            private int higherLimit;
            private int middleLimit;
            private int lowerLimit;
            private int higherRaise;
            private int middleRaise;
            private int lowerRaise;

            public int getHigherLimit() {
                return higherLimit;
            }

            public void setHigherLimit(int higherLimit) {
                this.higherLimit = higherLimit;
            }

            public int getMiddleLimit() {
                return middleLimit;
            }

            public void setMiddleLimit(int middleLimit) {
                this.middleLimit = middleLimit;
            }

            public int getLowerLimit() {
                return lowerLimit;
            }

            public void setLowerLimit(int lowerLimit) {
                this.lowerLimit = lowerLimit;
            }

            public int getHigherRaise() {
                return higherRaise;
            }

            public void setHigherRaise(int higherRaise) {
                this.higherRaise = higherRaise;
            }

            public int getMiddleRaise() {
                return middleRaise;
            }

            public void setMiddleRaise(int middleRaise) {
                this.middleRaise = middleRaise;
            }

            public int getLowerRaise() {
                return lowerRaise;
            }

            public void setLowerRaise(int lowerRaise) {
                this.lowerRaise = lowerRaise;
            }
        }
    }
}
