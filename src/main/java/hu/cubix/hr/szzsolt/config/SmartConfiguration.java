package hu.cubix.hr.szzsolt.config;

import hu.cubix.hr.szzsolt.service.EmployeeService;
import hu.cubix.hr.szzsolt.service.SmartEmployeeService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("smart")
public class SmartConfiguration {

    @Bean
    public EmployeeService employeeService() {
        return new SmartEmployeeService();
    }
}
