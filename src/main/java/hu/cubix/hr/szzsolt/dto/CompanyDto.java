package hu.cubix.hr.szzsolt.dto;

import com.fasterxml.jackson.annotation.JsonView;

import java.util.List;

public class CompanyDto {
    @JsonView(View.BaseData.class)
    private long id;
    @JsonView(View.BaseData.class)
    private int registrationNumber;
    @JsonView(View.BaseData.class)
    private String name;
    @JsonView(View.BaseData.class)
    private String address;
    private List<EmployeeDto> employeeList;

    public CompanyDto(long id, int registrationNumber, String name, String address, List<EmployeeDto> employeeList) {
        this.id = id;
        this.registrationNumber = registrationNumber;
        this.name = name;
        this.address = address;
        this.employeeList = employeeList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(int registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<EmployeeDto> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<EmployeeDto> employeeList) {
        this.employeeList = employeeList;
    }
}
