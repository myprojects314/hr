package hu.cubix.hr.szzsolt.mapper;

import hu.cubix.hr.szzsolt.dto.CompanyDto;
import hu.cubix.hr.szzsolt.dto.EmployeeDto;
import hu.cubix.hr.szzsolt.model.Company;
import hu.cubix.hr.szzsolt.model.Employee;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CompanyMapper {
    List<CompanyDto> companyToDtoList(List<Company> companyList);

    //@Mapping(target = "companyId", source = "id")
    CompanyDto companyToDto(Company company);

    @IterableMapping(qualifiedByName = "summary")
    Company dtoToCompany(CompanyDto companyDto);

    List<Company> dtoToCompanyList(List<CompanyDto> companyDtoList);

    @IterableMapping(qualifiedByName = "summary")
    List<CompanyDto> companyToSummaryDtoList(List<Company> company);

    @Mapping(target = "employeeList", ignore = true)
    @Named("summary")
    CompanyDto companyToSummaryDto(Company company);

    @Mapping(target = "title", source = "position.name")
    @Mapping(target = "entryDate", source = "dateOfStartWork")
    @Mapping(target = "company", ignore = true)
    EmployeeDto employeeToDto(Employee employee);

    @InheritInverseConfiguration
    Employee dtoToEmployee(EmployeeDto employeeDto);

    List<Employee> dtoToEmployeeList(List<EmployeeDto> employeeDtoList);
}
