package hu.cubix.hr.szzsolt.mapper;

import hu.cubix.hr.szzsolt.dto.EmployeeDto;
import hu.cubix.hr.szzsolt.model.Employee;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {
    List<EmployeeDto> employeeToDtoList(List<Employee> employeeList);

    @Mapping(target = "title", source = "position.name")
    @Mapping(target = "entryDate", source = "dateOfStartWork")
    @Mapping(target = "companyDto.employeeList", ignore = true)
    EmployeeDto employeeToDto(Employee employee);

    @InheritInverseConfiguration
    Employee dtoToEmployee(EmployeeDto employeeDto);

    List<Employee> dtoToEmployeeList(List<EmployeeDto> employeeDtoList);
}
