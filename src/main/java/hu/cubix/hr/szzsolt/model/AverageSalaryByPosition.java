package hu.cubix.hr.szzsolt.model;

public interface AverageSalaryByPosition {
    public String getPosition();

    public int getAverageSalary();
}
