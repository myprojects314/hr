package hu.cubix.hr.szzsolt.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

import java.time.LocalDateTime;

@Entity
public class Employee {
    @Id
    @GeneratedValue
    private long id;
    private String name;
    private int salary;
    private LocalDateTime dateOfStartWork;

    @ManyToOne
    private Company company;

    @ManyToOne
    private Position position;

    public Employee() {
    }
    public Employee(String name, int salary, LocalDateTime dateOfStartWork, Position position) {
        this(0, name, salary, dateOfStartWork, position);
    }
    public Employee(long id, String name, int salary, LocalDateTime dateOfStartWork, Position position) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.dateOfStartWork = dateOfStartWork;
        this.position = position;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public LocalDateTime getDateOfStartWork() {
        return dateOfStartWork;
    }

    public void setDateOfStartWork(LocalDateTime dateOfStartWork) {
        this.dateOfStartWork = dateOfStartWork;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
