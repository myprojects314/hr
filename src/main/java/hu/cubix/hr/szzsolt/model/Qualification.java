package hu.cubix.hr.szzsolt.model;

public enum Qualification {
    HIGH_SCHOOL, COLLEGE, UNIVERSITY, PHD;
}
