package hu.cubix.hr.szzsolt.repository;

import hu.cubix.hr.szzsolt.model.AverageSalaryByPosition;
import hu.cubix.hr.szzsolt.model.Company;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CompanyRepository extends JpaRepository<Company, Long> {
    @Query("SELECT DISTINCT c FROM Company c JOIN c.employeeList e WHERE e.salary > :minSalary")
    public List<Company> findByEmployeeWithSalaryHigherThan(int minSalary);

    @Query("SELECT c FROM Company c WHERE SIZE(c.employeeList) > :minEmployeeCount")
    public List<Company> findByEmployeeCountHigherThan(int minEmployeeCount);

    @Query("SELECT e.position.name AS position, avg(e.salary) AS averageSalary "
            + "FROM Company c "
            + "JOIN c.employeeList e "
            + "WHERE c.id=:companyId "
            + "GROUP BY e.position.name "
            + "ORDER BY avg(e.salary) DESC")
    public List<AverageSalaryByPosition> findAverageSalariesByPosition(long companyId);

    @Query("SELECT c FROM Company c")
    @EntityGraph(attributePaths = {"employeeList", "employeeList.position"})
    public List<Company> findAllWithEmployees();

    @Query("SELECT c FROM Company c WHERE c.id=:id")
    @EntityGraph(attributePaths = {"employeeList", "employeeList.position"})
    public Optional<Company> findByIdWithEmployees(long id);

}
