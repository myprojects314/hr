package hu.cubix.hr.szzsolt.repository;

import hu.cubix.hr.szzsolt.model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Long>, JpaSpecificationExecutor<Employee> {
    Page<Employee> findBySalaryGreaterThan(int minSalary, Pageable pageable);

    List<Employee> findByPositionName(String job);

    List<Employee> findByNameStartingWithIgnoreCase(String name);

    List<Employee> findByDateOfStartWorkBetween(LocalDateTime start, LocalDateTime end);

    @Modifying
    @Query("UPDATE Employee e "
            + "SET e.salary = :minSalary "
            + "WHERE e.position.name = :position "
            + "AND e.company.id = :companyId "
            + "AND e.salary < :minSalary")
    public void updateSalaries(long companyId, String position, int minSalary);
}
