package hu.cubix.hr.szzsolt.repository;

import hu.cubix.hr.szzsolt.model.Position;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PositionRepository extends JpaRepository<Position, Integer> {
    public List<Position> findByName(String name);
}
