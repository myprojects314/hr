package hu.cubix.hr.szzsolt.service;


import hu.cubix.hr.szzsolt.model.Company;
import hu.cubix.hr.szzsolt.model.Employee;
import hu.cubix.hr.szzsolt.repository.CompanyRepository;
import hu.cubix.hr.szzsolt.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {
    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    public Company save(Company company) {
        return companyRepository.save(company);
    }

    public Company update(Company company) {
        if (!companyRepository.existsById(company.getId()))
            return null;
        return companyRepository.save(company);
    }

    public List<Company> findAll(boolean full) {
        if (!full) {
            return companyRepository.findAll();
        } else {
            return companyRepository.findAllWithEmployees();
        }
    }

    public Optional<Company> findById(long id, boolean full) {
        if (!full) {
            return companyRepository.findById(id);
        } else {
            return companyRepository.findByIdWithEmployees(id);
        }
    }

    public void delete(long id) {
        companyRepository.deleteById(id);
    }

    public Company addEmployee(long id, Employee employee) {
        Company company = companyRepository.findById(id).get();
        company.addEmployee(employee);
        employeeRepository.save(employee);
        return company;
    }

    public Company deleteEmployee(long id, long employeeId) {
        Company company = companyRepository.findById(id).get();
        Employee employee = employeeRepository.findById(employeeId).get();
        employee.setCompany(null);
        company.getEmployeeList().remove(employee);
        employeeRepository.save(employee);
        return company;
    }

    public Company replaceEmployees(long id, List<Employee> employees) {
        Company company = companyRepository.findById(id).get();
        for (Employee employee : company.getEmployeeList()) {
            employee.setCompany(null);
        }
        company.getEmployeeList().clear();
        for (Employee employee : employees) {
            company.addEmployee(employee);
            employeeRepository.save(employee);
        }
        return company;
    }
}
