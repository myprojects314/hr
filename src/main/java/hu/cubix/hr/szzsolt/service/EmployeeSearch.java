package hu.cubix.hr.szzsolt.service;

import hu.cubix.hr.szzsolt.model.Employee;
import hu.cubix.hr.szzsolt.repository.EmployeeRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeSearch {

    @Autowired
    EmployeeRepository employeeRepository;

    @Transactional
    public Employee findEmployeeByExample(Employee employee) {
        return employeeRepository.findById(employee.getId()).get();
    }
}
