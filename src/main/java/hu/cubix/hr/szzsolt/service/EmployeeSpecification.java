package hu.cubix.hr.szzsolt.service;

import hu.cubix.hr.szzsolt.model.Employee;
import hu.cubix.hr.szzsolt.model.Position;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;

public class EmployeeSpecification {

    public static Specification<Employee> hasId(long id) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("id"), id);
    }

    public static Specification<Employee> employeeNameStartsWith(String prefix) {
        return (root, query, cB) -> cB.like(cB.lower(root.get("name")), prefix.toLowerCase() + "%");
    }

    public static Specification<Employee> matchPositions(Position position) {
        return (root, query, cB) -> cB.equal(root.get("position"), position);
    }

    public static Specification<Employee> salaryAround(int salary) {
        double lowerLimit = salary - (salary * 0.95);
        double higherLimit = salary + (salary * 0.95);
        return (root, query, cB) -> cB.between(root.get("salary"), lowerLimit, higherLimit);
    }

    public static Specification<Employee> matchDate(LocalDate date) {
        return (root, query, cB) -> cB.equal(root.get("dateOfStartWork"), date);
    }

    public static Specification<Employee> companyNameStartsWith(String prefix) {
        return (root, query, cB) -> cB.like(cB.lower(root.get("company")), prefix.toLowerCase() + "%");
    }
}
