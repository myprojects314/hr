package hu.cubix.hr.szzsolt.service;

import hu.cubix.hr.szzsolt.model.*;
import hu.cubix.hr.szzsolt.repository.CompanyRepository;
import hu.cubix.hr.szzsolt.repository.EmployeeRepository;
import hu.cubix.hr.szzsolt.repository.PositionDetailsByCompanyRepository;
import hu.cubix.hr.szzsolt.repository.PositionRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class InitDbService {
    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    PositionRepository positionRepository;

    @Autowired
    PositionDetailsByCompanyRepository positionDetailsByCompanyRepository;

    @Transactional
    public void initDb() {

        Position developer = positionRepository.save(new Position("developer", Qualification.UNIVERSITY));
        Position tester = positionRepository.save(new Position("tester", Qualification.HIGH_SCHOOL));

        Employee newEmployee1 = employeeRepository.save(new Employee(1,"Nagy Attila", 250000,LocalDateTime.now(),null));
        Employee newEmployee2 = employeeRepository.save(new Employee(2, "Kis Ferenc", 200000, LocalDateTime.now(), null));

        Company newCompany = companyRepository.save(new Company(1, 10, "Example", "", null));
        newCompany.addEmployee(newEmployee2);
        newCompany.addEmployee(newEmployee1);

        PositionDetailsByCompany pd = new PositionDetailsByCompany();
        pd.setCompany(newCompany);
        pd.setMinSalary(250000);
        pd.setPosition(developer);
        positionDetailsByCompanyRepository.save(pd);

        PositionDetailsByCompany pd2 = new PositionDetailsByCompany();
        pd2.setCompany(newCompany);
        pd2.setMinSalary(200000);
        pd2.setPosition(tester);
        positionDetailsByCompanyRepository.save(pd2);
    }
}
