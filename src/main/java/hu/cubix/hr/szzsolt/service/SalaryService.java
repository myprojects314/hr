package hu.cubix.hr.szzsolt.service;

import hu.cubix.hr.szzsolt.model.Employee;
import hu.cubix.hr.szzsolt.repository.EmployeeRepository;
import hu.cubix.hr.szzsolt.repository.PositionDetailsByCompanyRepository;
import hu.cubix.hr.szzsolt.repository.PositionRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class SalaryService {

    private EmployeeService employeeService;
    private PositionRepository positionRepository;
    private PositionDetailsByCompanyRepository positionDetailsByCompanyRepository;
    private EmployeeRepository employeeRepository;

    public SalaryService(EmployeeService employeeService, PositionRepository positionRepository,
                         PositionDetailsByCompanyRepository positionDetailsByCompanyRepository,
                         EmployeeRepository employeeRepository) {
        super();
        this.employeeService = employeeService;
        this.positionRepository = positionRepository;
        this.positionDetailsByCompanyRepository = positionDetailsByCompanyRepository;
        this.employeeRepository = employeeRepository;
    }

    public void setNewSalary(Employee employee) {
        int newSalary = employee.getSalary() * (100 + employeeService.getPayRaisePercent(employee)) / 100;
        employee.setSalary(newSalary);
    }

    @Transactional
    public void raiseMinSalary(long companyId, String position, int minSalary) {
        positionDetailsByCompanyRepository.findByPositionNameAndCompanyId(position, companyId)
                .forEach(pd -> {
                    pd.setMinSalary(minSalary);
                    employeeRepository.updateSalaries(companyId, position, minSalary);
                });
    }
}
