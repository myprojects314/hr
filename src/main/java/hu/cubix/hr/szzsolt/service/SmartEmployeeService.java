package hu.cubix.hr.szzsolt.service;

import hu.cubix.hr.szzsolt.config.HRConfigurationProperties;
import hu.cubix.hr.szzsolt.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Duration;
import java.time.LocalDateTime;


public class SmartEmployeeService extends AbstractEmployeeService {

    @Autowired
    private HRConfigurationProperties config;

    @Override
    public int getPayRaisePercent(Employee employee) {
        HRConfigurationProperties.Raise.Special special = config.getRaise().getSpecial();

        Duration duration = Duration.between(employee.getDateOfStartWork(), LocalDateTime.now());
        int hours = (int) duration.toHours();
        int years = hours / 8760;

        if (years >= special.getHigherLimit()) return special.getHigherRaise();
        else if (years >= special.getMiddleLimit()) return special.getMiddleRaise();
        else if (years >= special.getLowerRaise()) return special.getLowerRaise();
        else return 0;
    }
}
