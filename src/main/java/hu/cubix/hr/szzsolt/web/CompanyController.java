package hu.cubix.hr.szzsolt.web;

import hu.cubix.hr.szzsolt.dto.CompanyDto;
import hu.cubix.hr.szzsolt.dto.EmployeeDto;
import hu.cubix.hr.szzsolt.mapper.CompanyMapper;
import hu.cubix.hr.szzsolt.model.AverageSalaryByPosition;
import hu.cubix.hr.szzsolt.model.Company;
import hu.cubix.hr.szzsolt.repository.CompanyRepository;
import hu.cubix.hr.szzsolt.service.CompanyService;
import hu.cubix.hr.szzsolt.service.SalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/companies")
public class CompanyController {
    @Autowired
    private CompanyMapper companyMapper;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private SalaryService salaryService;

    @GetMapping
    public List<CompanyDto> findAllCompany(@RequestParam Optional<Boolean> full) {
        List<Company> companies = companyService.findAll(full.orElse(false));
        return mapCompanies(companies, full);
    }

    @GetMapping("/{id}")
    public CompanyDto findCompany(@RequestParam Optional<Boolean> full, @PathVariable long id) {
        Company company = getCompanyOrThrow(id, full.orElse(false));

        if (full.orElse(false)) {
            return companyMapper.companyToDto(company);
        } else {
            return companyMapper.companyToSummaryDto(company);
        }
    }

    @GetMapping(params = "aboveSalary")
    public List<CompanyDto> getCompaniesAboveSalary(@RequestParam int aboveSalary, @RequestBody Optional<Boolean> full) {
        List<Company> filteredCompanies = companyRepository.findByEmployeeWithSalaryHigherThan(aboveSalary);
        return mapCompanies(filteredCompanies, full);
    }

    @GetMapping(params = "aboveEmployeeCount")
    public List<CompanyDto> getCompaniesAboveEmployeeCount(@RequestParam int aboveEmployeeCount,
                                                           @RequestBody Optional<Boolean> full) {
        List<Company> filteredCompanies = companyRepository.findByEmployeeCountHigherThan(aboveEmployeeCount);
        return mapCompanies(filteredCompanies, full);
    }

    @GetMapping("/{id}/salaryStats")
    public List<AverageSalaryByPosition> getSalaryStatsById(@PathVariable long id) {
        return companyRepository.findAverageSalariesByPosition(id);
    }

    @PutMapping("/{id}/raiseMin/{position}/{minSalary}")
    public void raiseMinSalary(@PathVariable long id, @PathVariable String position, @PathVariable int minSalary) {
        salaryService.raiseMinSalary(id, position, minSalary);
    }

    @PostMapping
    public CompanyDto createCompany(@RequestBody CompanyDto companyDto) {
        return companyMapper.companyToDto(companyService.save(companyMapper.dtoToCompany(companyDto)));
    }

    @PutMapping("/{id}")
    public CompanyDto updateCompany(@PathVariable long id, @RequestBody CompanyDto companyDto) {
        companyDto.setId(id);
        Company updateCompany = companyService.update(companyMapper.dtoToCompany(companyDto));

        if (updateCompany == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        else {
            return companyMapper.companyToDto(updateCompany);
        }
    }

    @DeleteMapping("/{id}")
    public void deleteCompany(@PathVariable long id) {
        companyService.delete(id);
    }

    @PostMapping("/{id}/employees")
    public CompanyDto addNewEmployee(@PathVariable long id, @RequestBody EmployeeDto employeeDto) {
        Company company = companyService.addEmployee(id, companyMapper.dtoToEmployee(employeeDto));
        return companyMapper.companyToDto(company);
    }

    @PutMapping("/{id}/employee")
    public CompanyDto replaceAllEmployee(@PathVariable long id, List<EmployeeDto> employeeDtoList) {
        Company company = companyService.replaceEmployees(id, companyMapper.dtoToEmployeeList(employeeDtoList));
        return companyMapper.companyToDto(company);
    }

    @DeleteMapping("/{id}/employees/{employeeId}")
    public CompanyDto deleteEmployee(@PathVariable long id, @PathVariable long employeeId) {
        Company company = companyService.deleteEmployee(id, employeeId);
        return companyMapper.companyToDto(company);
    }

    private List<CompanyDto> mapCompanies(List<Company> company, Optional<Boolean> full) {
        if (full.orElse(false)) {
            return companyMapper.companyToDtoList(company);
        } else
            return companyMapper.companyToSummaryDtoList(company);
    }

    private Company getCompanyOrThrow(long id, boolean full) {
        return companyService.findById(id, full).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
}
