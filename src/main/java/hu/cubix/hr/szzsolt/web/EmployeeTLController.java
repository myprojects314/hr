package hu.cubix.hr.szzsolt.web;

import hu.cubix.hr.szzsolt.model.Employee;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class EmployeeTLController {
    private List<Employee> allEmployees = new ArrayList<>();

    @GetMapping("/employees")
    public String listEmployees(Map<String, Object> model) {
        model.put("employees", allEmployees);
        model.put("newEmployee", new Employee());
        return "employees";
    }

    @PostMapping("/employees")
    public String addEmployee(Employee employee) {
        allEmployees.add(employee);
        return "redirect:employees";
    }

    @GetMapping("/deleteEmployee/{id}")
    public String deleteEmployee(@PathVariable long id) {
        allEmployees.removeIf(e -> e.getId() == id);
        return "redirect:/employees";
    }

    @GetMapping("/employees/{id}")
    public String editEmployee(@PathVariable long id, Map<String, Object> model) {
        model.put("editableEmployee", allEmployees.stream()
                .filter(e -> e.getId() == id).findFirst().get());
        return "newEmployee";
    }

    @PostMapping("/modifyEmployee")
    public String modifyEmployee(Employee employee) {
        for (int i = 0; i < allEmployees.size(); ++i) {
            if (allEmployees.get(i).getId() == employee.getId()) {
                allEmployees.set(i, employee);
                break;
            }
        }
        return "redirect:employees";
    }
}
