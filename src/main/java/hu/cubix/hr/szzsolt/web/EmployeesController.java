package hu.cubix.hr.szzsolt.web;

import hu.cubix.hr.szzsolt.dto.EmployeeDto;
import hu.cubix.hr.szzsolt.mapper.EmployeeMapper;
import hu.cubix.hr.szzsolt.model.Employee;
import hu.cubix.hr.szzsolt.repository.EmployeeRepository;
import hu.cubix.hr.szzsolt.service.EmployeeService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

@RestController
@RequestMapping("/api/employees")
public class EmployeesController {
    private Map<Long, EmployeeDto> employeeMap = new HashMap<>();

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping
    public List<EmployeeDto> findAll(@RequestParam Optional<Integer> minSalary, @SortDefault("id") Pageable pageable) {
        List<Employee> employeeList = null;
        if (minSalary.isPresent()) {
            Page<Employee> employeePage = employeeRepository.findBySalaryGreaterThan(minSalary.get(), pageable);
            employeeList = employeePage.getContent();
        } else {
            employeeList = employeeService.findAll();
        }
        return employeeMapper.employeeToDtoList(employeeList);
    }

    @GetMapping("/{id}")
    public EmployeeDto findId(@PathVariable long id) {
        Employee employee = employeeService.findById(id).get();
        if (employee == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        return employeeMapper.employeeToDto(employee);
    }

    @PostMapping
    public EmployeeDto create(@RequestBody @Valid EmployeeDto employeeDto) {
        return employeeMapper.employeeToDto(employeeService.save(employeeMapper.dtoToEmployee(employeeDto)));
    }

    @PutMapping("/{id}")
    public EmployeeDto update(@PathVariable long id, @RequestBody @Valid EmployeeDto employeeDto) {
        employeeDto.setId(id);
        Employee updateEmployee = employeeService.update(employeeMapper.dtoToEmployee(employeeDto));

        if (updateEmployee == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        else {
            return employeeMapper.employeeToDto(updateEmployee);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        employeeService.delete(id);
    }

    @PostMapping("/payRaise")
    private int getPayRaisePercent(@RequestBody Employee employee) {
        return employeeService.getPayRaisePercent(employee);
    }
}
