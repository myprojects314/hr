package hu.cubix.hr.szzsolt.web;

import hu.cubix.hr.szzsolt.model.Employee;
import hu.cubix.hr.szzsolt.model.Position;
import hu.cubix.hr.szzsolt.model.Qualification;
import hu.cubix.hr.szzsolt.repository.EmployeeRepository;
import hu.cubix.hr.szzsolt.service.EmployeeSearch;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@AutoConfigureTestDatabase
public class EmployeeSearchTest {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    EmployeeSearch employeeSearch;

    @Test
    void testFindEmployeeByExample() throws Exception {
        Position position = new Position("CEO", Qualification.PHD);

        Employee employee1 = employeeRepository.save(new Employee("Max Lee", 5000, LocalDateTime.now(), position));

        Employee savedEmployee1 = employeeSearch.findEmployeeByExample(employee1);

        assertThat(savedEmployee1.getId() == employee1.getId());
        assertThat(savedEmployee1.getName().equals(employee1.getName()));
        assertThat(savedEmployee1.getPosition().getName().equals(employee1.getPosition().getName()));

        double lowerLimit = savedEmployee1.getSalary() - (savedEmployee1.getSalary() * 0.95);
        double higherLimit = savedEmployee1.getSalary() + (savedEmployee1.getSalary() * 0.95);
        assertThat(savedEmployee1.getSalary() > lowerLimit && savedEmployee1.getSalary() < higherLimit);

        LocalDateTime localDateTimeEmployee1 = employee1.getDateOfStartWork();
        LocalDate employeeDate = localDateTimeEmployee1.toLocalDate();
        LocalDateTime localDateTimeSavedEmployee1 = savedEmployee1.getDateOfStartWork();
        LocalDate savedEmployeeDate = localDateTimeSavedEmployee1.toLocalDate();
        assertThat(savedEmployeeDate.equals(employeeDate));

    }
}
